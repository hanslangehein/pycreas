"""
    MathFunctions: A Python module containing mathematical functions.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import math

from numpy import arctan2, pi


def weakly_increasing(vals):

    """
    Check if list is 'just' increasing.
    """

    return all(ii <= jj for ii, jj in zip(vals, vals[1:]))


def weakly_decreasing(vals):

    """
    Check if list is 'just' decreasing.
    """

    return all(ii >= jj for ii, jj in zip(vals, vals[1:]))


def get_dot_product(r0, r1):

    """
    Returns the scalar / dot product of given pair of nD-vectors
    """

    # Check input dimensions
    if len(r0) != len(r1):
        raise Exception("Dimensions mismatch for the given vectors")

    # Scalar / dot product
    dot = sum([dim0 * dim1 for dim0, dim1 in zip(r0, r1)])

    return dot


def get_determinant(r0, r1):

    """
    Returns the determinant of given pair of 2D-vectors
    """

    # Check input dimensions
    if len(r0) != 2 or len(r1) != 2:
        raise Exception("Incompatible dimensions: dimension must be 2")

    # Determinant
    det = r0[0] * r1[1] - r0[1] * r1[0]

    return det


def cart_to_polar(r0):

    """
    Converts given 2D-vector from Cartesian to polar coordinates.
    """

    xx, yy = r0

    rad = (xx**2 + yy**2) ** 0.5
    ang = arctan2(yy, xx)

    return [rad, ang]


def polar_to_cart(r0):
    """
    Converts given 2D-vector from polar to Cartesian coordinates.
    """

    rad, ang = r0

    xx = rad * math.cos(ang)
    yy = rad * math.sin(ang)

    return [xx, yy]


def normalize_angle_small(ang):

    """
    Converts given angle to an angle between -pi < ang <= pi.
    """

    while ang <= -pi:
        ang += 2 * pi

    while ang > pi:
        ang -= 2 * pi

    return ang


def get_orientation(pA, pB, pC):

    """
    Returns the orientation of given triplet of 2D-points.

    det( A , B , C ) =

      | xA xB xC |   | xA xB-xA xC-xA |
    = | yA yB yC | = | yA yB-yA yC-yA | = (xB-xA)(yC-yA) - (xC-xA)(yB-yA)
      |  1  1  1 |   |  1     0     0 |

    """

    # Vectors
    r_AB = [dimB - dimA for dimA, dimB in zip(pA, pB)]
    r_AC = [dimC - dimA for dimA, dimC in zip(pA, pC)]

    # Determinant
    det = get_determinant(r_AB, r_AC)

    # Points constitute a counter-clockwise turn
    if det > 0:
        return 1

    # Points constitute a clockwise turn
    elif det < 0:
        return -1

    # Points are collinear
    else:
        return 0


def get_intersection_lines(pA, pB, pC, pD):

    """
    Returns the intersection points of a pair of lines each defined by given
    pair of 2D-points.

    Intersection point pE is computed using the parametric equation of lines:

        pE = pA + u * r_AB
        = pC + v * r_CD
    <=> u * r_AB - v * r_CD = r_AC
    <=> (r_AB , r_CD) * (u , -v)^T = r_AC

    A system of linear equations for the variables u and v is obtained:

        (r_AB , r_CD) = r_AC

    Using explicit matrix inversion, the first line yields u:

        u = 1/|r_AB , r_CD| * (r_CD[1]*r_AC[0] - r_CD[0]*r_AC[1])
          = |r_AC , r_CD| / |r_AB , r_CD|

    """

    # Vectors
    r_AB = [dimB - dimA for dimA, dimB in zip(pA, pB)]
    r_CD = [dimD - dimC for dimC, dimD in zip(pC, pD)]
    r_AC = [dimC - dimA for dimA, dimC in zip(pA, pC)]

    # Check lines
    if r_AB[0] == 0 and r_AB[1] == 0 or r_CD[0] == 0 and r_CD[1] == 0:
        raise Exception(
            "At least one pair of points for defining a line are coincident"
        )

    # Check for linear independence using the determinant
    det = get_determinant(r_AB, r_CD)
    if det == 0:
        if get_orientation(pA, pB, pC) == 0:
            raise Exception(
                "Lines are coincident: infinite number of solutions"
            )
        else:
            raise Exception("Lines are parallel: no solution")

    # Compute u using the explicit matrix inverse
    u = get_determinant(r_AC, r_CD) / det

    # Compute intersection point
    pE = [dimA + u * dimB for dimA, dimB in zip(pA, r_AB)]

    return pE


def get_polygon_center(pnts):

    # Loop through polygon's vertices
    ww = []
    midpnts = []
    for ii in range(len(pnts)):

        # Length of polygon's sides
        p0, p1 = pnts[ii - 1], pnts[ii]
        r_01 = [dim1 - dim0 for dim0, dim1 in zip(p0, p1)]
        ww.append(get_dot_product(r_01, r_01) ** 0.5)

        # Midpoints of polygon's sides (times weight)
        midpnts.append(
            [(dim0 + dim1) / 2 * ww[ii] for dim0, dim1 in zip(p0, p1)]
        )

    # Weighted arithmetic mean
    midpnts_transp = list(map(list, zip(*midpnts)))
    cntr = [sum(dim) / sum(ww) for dim in midpnts_transp]

    return cntr
