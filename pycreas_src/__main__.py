#!/usr/bin/env python3

"""
    PyCreas: A tool for quantification of localization and distribution of endocrine cell types in the islets of Langerhans.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    PyCreas searches for images in a given target folder, obtains and analyzes
    information contained in the images, and saves the results in a folder
    located in the pycreas_src directory.

    To obtain and analyze information, the following subprograms are used:
    - CellDetection: Locates pixels of a given image using the HSV color space.
    - IsletGui: Edits, saves, and loads data associated with a given image.
    - RelativeRadius: Calculates the relative radius of points within a polygon.

    The process of obtaining information from images of islets of Langerhans is
    semi-automatic and organized as follows:
    1) Automatic detection of cells in the image (see CellDetection).
    2) Manual definition of an islet border (see IsletGui).

    The process of analyzing obtained information corresponding to a selected
    islet of Langerhans includes:
    1) Updating of detected cells by excluding positions outside the islet.
    2) Counting of cell and islet pixels to obtain respective area measurements.
    3) Calculation of relative area: ratio between cell pixel area and islet
    pixel area.
    4) Calculation of relative radius for each cell pixel (see RelativeRadius).
    5) Arithmetic averaging of relative radii less than or equal to 100% to
    obtain an islet-specific quantification for the cell distribution.

    Regarding step 5, it should be noted why relative radii greater than 100%
    are excluded from averaging because, after all, the relative radius is
    calculated exclusively for cells within the islet (see step 1). The
    occurrence of relative radii greater than 100% is due to a modification of
    the original islet shape. This modification is performed only for the
    calculation of the relative radii. The modification is necessary by
    definition and originates in making the islet shape valid (see
    RelativeRadius). How this affects the shape of an islet must be evaluated
    individually for each islet. For this purpose, the percentage of excluded
    relative radii is calculated to assess whether an islet is qualified for
    this method. If the excluded relative radii percentage is low, only a small
    proportion of cells are within invalid islet regions. Accordingly, only a
    small proportion of relative radii are excluded from averaging, indicating
    that the result is a representative quantification for the cell distribution.

    The results are processed into a table with rows corresponding to the
    different images and columns representing the following information:
    - Cell pixel area
    - Islet pixel area
    - Cell-islet area ratio
    - Average relative radius
    - Excluded relative radii percentage

    This table is then saved in the results folder as a CSV textfile and as an
    XLSX spreadsheet. It is also used to generate plots, where each column
    corresponds to a different plot. Additionally, plots are generated to
    visualize the obtained information from the last image in the specified
    target folder.

    Warnings are issued if the set of detected cells and the cells inside the
    islet are disjoint.
"""

import argparse
import os.path
import re

import numpy as np
import cv2
import matplotlib.pyplot as pl
import matplotlib.colors as mcolors
from openpyxl import load_workbook, Workbook

from cell_detection import CellDetection
from islet_gui import IsletGui
from relative_radius import RelativeRadius
import common


def scale_rad_to_img(rad, img, max_radius):
    return rad / max_radius * img.shape[0]


def scale_ang_to_img(ang, img):

    ang = np.copy(ang)
    ang = -ang
    ang[ang < 0] += 2 * np.pi
    return ang / (2 * np.pi) * img.shape[1]


def main():

    # Path to results directory
    dirpath_results = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "_results")
    )

    # Path to images directory
    dirpath_img = os.path.abspath(args.path)
    if os.path.isdir(dirpath_img):
        logger.info(f"Searching for images in folder '{dirpath_img}'")
    else:
        logger.error(f"No such folder: '{dirpath_img}'")
        return

    # Name of images directory
    dirname_img = re.sub(r"\W", "", os.path.basename(dirpath_img))

    # Path to plots directory
    dirpath_plots = os.path.join(dirpath_results, dirname_img)

    # Create IsletGui object
    islet_gui = IsletGui()

    # List files
    images = []
    for file in sorted(os.listdir(dirpath_img)):
        filepath_img = os.path.join(dirpath_img, file)
        if os.path.isfile(filepath_img):
            if filepath_img != common.replace_extension(filepath_img, "csv"):
                images.append(filepath_img)

    # Check if images can be opened
    images_check = []
    for filepath_img in images:
        orig_img = common.my_imread(filepath_img)
        if orig_img is None:
            logger.debug(f"Not an image: '{filepath_img}'")
        else:
            images_check.append(filepath_img)

    # Print info
    logger.info(
        f"{len(images_check)}/{len(images)} files were accepted as images"
    )

    # Edit
    if not args.process_only:

        # Print info
        if args.read_only:
            logger.info(f"Editing '{dirname_img}' (modifications disabled)")
        else:
            logger.info(f"Editing '{dirname_img}' (modifications enabled)")

        # Loop through files
        for count, filepath_img in enumerate(images_check):

            # Edit data
            islet_gui.edit(filepath_img, args.read_only)

            # Print info
            logger.info(f"Progress: {count + 1}/{len(images_check)}")

        # Print info
        logger.info("Editing done")

    # Process
    if not args.edit_only:

        # Print info
        logger.info(f"Processing '{dirname_img}'")

        # Initialize results matrix and header
        data = np.full((len(images_check), 5), np.nan)
        header = [
            "Cell pixel count",
            "Islet pixel count",
            "Cell-islet area ratio",
            "Average relative radius",
            "Excluded relative radii percentage",
        ]

        # Loop through files
        for count, filepath_img in enumerate(images_check):

            # Get islet vertices
            returncode, isl_pnts = islet_gui.load(filepath_img)
            if returncode:
                continue

            # Open image
            orig_img = common.my_imread(filepath_img)

            # Get cell mask
            cell_dect = CellDetection(orig_img)
            cell_mask = cell_dect.get_mask()

            # Create mask from islet vertices
            isl_mask = np.zeros(np.shape(orig_img)[:2], dtype=np.uint8)
            cv2.fillPoly(isl_mask, [isl_pnts], 255)

            # Update cell mask
            cell_mask = cv2.bitwise_and(cell_mask, isl_mask)

            # Get cell points
            cell_pnts = np.array(np.where(cell_mask))
            cell_pnts = np.array([cell_pnts[1, :], cell_pnts[0, :]]).T

            # Get actual islet points (not vertices)
            act_isl_pnts = np.array(np.where(isl_mask))
            act_isl_pnts = np.array([act_isl_pnts[1, :], act_isl_pnts[0, :]]).T

            # Get cell and islet area
            cell_area = np.shape(cell_pnts)[0]
            isl_area = np.shape(act_isl_pnts)[0]

            # Compute relative area
            rel_area = cell_area / isl_area

            # Create RelativeRadius object
            rel_rad = RelativeRadius()

            # Perform calculation
            returncode, results = rel_rad.main(cell_pnts, isl_pnts)
            if returncode & rel_rad.INPUT_ERROR:
                logger.warning(
                    f"No cells found or encircled in image '{filepath_img}'"
                )

            # Unpack results
            (
                rel_radii,
                cntr,
                pnts_polar,
                poly_modified,
                poly_polar,
                proj,
                proj_polar,
            ) = results

            # Exclude relative radii greater than 100%
            rel_radii_incl = rel_radii[rel_radii <= 1]
            rel_radii_excl = rel_radii[rel_radii > 1]
            perc_excl = np.size(rel_radii_excl) / np.size(rel_radii) * 100

            # Compute average relative radius
            if np.size(rel_radii_incl):
                avg_rel_radius = np.mean(rel_radii_incl)
            else:
                avg_rel_radius = None

            # Store data
            data[count, :] = np.array(
                [cell_area, isl_area, rel_area, avg_rel_radius, perc_excl]
            )

        # Print info
        logger.info("Processing done")

        # Create results directory
        if not os.path.isdir(dirpath_results):
            os.makedirs(dirpath_results)
            logger.info(f"Created folder(s) '{dirpath_results}'")

        # Write data to csv
        logger.info("Saving data ...")
        filepath_csv = os.path.join(dirpath_results, f"{dirname_img}.csv")
        with open(filepath_csv, mode="w", encoding="utf-8") as csv_file:
            np.savetxt(
                csv_file,
                data,
                delimiter=";",
                header=";".join(header),
                encoding="utf-8",
            )
        logger.info(f"Saved data to '{filepath_csv}'")

        # Write data to xlsx
        filepath_xlsx = os.path.join(dirpath_results, "data.xlsx")
        if os.path.isfile(filepath_xlsx):
            workbook = load_workbook(filename=filepath_xlsx)
            if dirname_img in workbook.sheetnames:
                workbook.remove(workbook[dirname_img])
        else:
            workbook = Workbook()
            workbook.remove(workbook.active)
        sheet = workbook.create_sheet(dirname_img)
        sheet.append(["File index", "File name"] + header)
        for count, filepath_img in enumerate(images_check):
            title = os.path.basename(filepath_img)
            sheet.append([count + 1, title] + data[count, :].tolist())
        workbook.save(filepath_xlsx)
        logger.info(f"Saved data to '{filepath_xlsx}'")

        # Create plots from data
        axes = []
        for ii in range(np.shape(data)[1]):
            ax = pl.subplots(layout="constrained")[1]
            ax.plot(
                np.arange(1, len(images_check) + 1),
                data[:, ii],
                linestyle="dotted",
                marker=".",
                linewidth=1,
                color="0.3",
            )
            ax.set_title(header[ii], color="1")
            ax.set_xlabel("File index")
            ax.set_ylabel(header[ii])
            ax.grid("true")
            axes.append(ax)

        # Check if we can plot last image
        if np.size(data) and not np.any(np.isnan(data[-1, :])):

            # Create masked image
            color_array = [ii * 255 for ii in mcolors.to_rgb("yellowgreen")]
            dect_img = cell_dect.apply_mask(cell_mask, color_array)

            # Open additional support images
            returncode, (_, spprt_img) = islet_gui._open_support(filepath_img)

            # Initialize axes
            kwargs = dict(
                sharey=True,
                layout="constrained",
                squeeze=False,
                figsize=(6.4, 4.8),
            )
            axes_res = [pl.subplots(**kwargs)[1] for _ in range(4)]
            axes_support = pl.subplots(**kwargs)[1]
            if returncode & islet_gui.OPEN_SUPPORT_IMAGE_ERROR:
                ncols = 2
                kwargs["figsize"] = (4, 2)
            else:
                ncols = 3
                kwargs["figsize"] = (5.5, 2)
            axes_showcase = pl.subplots(1, ncols, **kwargs)[1]

            # Flatten axes
            axes_res = np.reshape(axes_res, -1)
            axes_showcase = np.reshape(axes_showcase, -1)
            axes_support = np.reshape(axes_support, -1)

            # Store axes
            axes += axes_res.tolist()
            axes += axes_showcase.tolist()
            if not returncode & islet_gui.OPEN_SUPPORT_IMAGE_ERROR:
                axes += axes_support.tolist()

            # Show original image
            axes_showcase[0].imshow(orig_img, cmap="gray")

            # Show support image
            if not returncode & islet_gui.OPEN_SUPPORT_IMAGE_ERROR:
                for ax in [axes_showcase[1], axes_support[0]]:
                    ax.imshow(spprt_img, cmap="gray")

            # Plot polygon
            for ax in np.append(
                [axes_res[0], axes_support[0]], axes_showcase[1:]
            ):
                ax.plot(
                    np.append(isl_pnts[:, 0], isl_pnts[0, 0]),
                    np.append(isl_pnts[:, 1], isl_pnts[0, 1]),
                    linestyle="dashed",
                    linewidth=1,
                    color="deepskyblue",
                    label="poly",
                )

            # Results in Cartesian plane
            for ax in [axes_res[0], axes_showcase[-1]]:

                # Show masked image
                ax.imshow(dect_img, cmap="gray")

                # Plot lines between center and projections
                skip = np.nonzero(rel_radii <= np.percentile(rel_radii, 25))[0]
                skip = skip[:: int(len(skip) // 6)]
                for pp in proj[skip, :]:
                    ax.plot(
                        [cntr[0], pp[0]],
                        [cntr[1], pp[1]],
                        linestyle="dotted",
                        linewidth=1,
                        color="1",
                    )

                # Plot points
                ax.plot(
                    cell_pnts[skip, 0],
                    cell_pnts[skip, 1],
                    linestyle="",
                    marker=".",
                    markersize=6,
                    markeredgecolor="1",
                    color="yellowgreen",
                    label="pnts",
                )

                # Plot center
                ax.plot(
                    cntr[0],
                    cntr[1],
                    linestyle="",
                    marker="+",
                    markersize=12,
                    color="red",
                    label="cntr",
                )

                # Plot projections
                ax.plot(
                    proj[skip, 0],
                    proj[skip, 1],
                    linestyle="",
                    marker=".",
                    markersize=6,
                    markeredgecolor="1",
                    color="royalblue",
                    label="proj",
                )

            # Results in polar plane
            max_radius = 2 * np.max(pnts_polar[:, 0])
            dect_img_polar = cv2.linearPolar(
                dect_img, cntr, max_radius, cv2.WARP_POLAR_LINEAR
            )
            dect_img_polar = cv2.resize(dect_img_polar, dect_img.shape[:-1])
            dect_img_polar = cv2.rotate(dect_img_polar, cv2.ROTATE_90_CLOCKWISE)
            axes_res[1].imshow(dect_img_polar, cmap="gray", origin="lower")
            axes_res[1].plot(
                scale_ang_to_img(pnts_polar[[0], 1], dect_img_polar),
                scale_rad_to_img(
                    pnts_polar[[0], 0], dect_img_polar, max_radius
                ),
                linestyle="",
                marker=".",
                markersize=1,
                color="yellowgreen",
                label="pnts",
            )
            axes_res[1].plot(
                scale_ang_to_img(poly_polar[:, 1], dect_img_polar),
                scale_rad_to_img(poly_polar[:, 0], dect_img_polar, max_radius),
                linestyle="",
                marker=".",
                markersize=1,
                color="deepskyblue",
                label="poly",
            )
            axes_res[1].plot(
                scale_ang_to_img(proj_polar[:, 1], dect_img_polar),
                scale_rad_to_img(proj_polar[:, 0], dect_img_polar, max_radius),
                linestyle="",
                marker=".",
                markersize=1,
                color="royalblue",
                label="proj",
            )

            # Absolute radii over angles
            axes_res[2].plot(
                pnts_polar[:, 1] * 180 / np.pi,
                pnts_polar[:, 0],
                linestyle="",
                marker=".",
                markersize=1,
                color="yellowgreen",
                label="pnts",
            )
            axes_res[2].plot(
                poly_polar[:, 1] * 180 / np.pi,
                poly_polar[:, 0],
                linestyle="",
                marker=".",
                markersize=1,
                color="deepskyblue",
                label="poly",
            )
            axes_res[2].plot(
                proj_polar[:, 1] * 180 / np.pi,
                proj_polar[:, 0],
                linestyle="",
                marker=".",
                markersize=1,
                color="royalblue",
                label="proj",
            )

            # Relative radii over angles
            axes_res[3].plot(
                pnts_polar[:, 1] * 180 / np.pi,
                rel_radii * 100,
                linestyle="",
                marker=".",
                markersize=1,
                color="goldenrod",
                label="rel_radii",
            )

            # Figure setup
            kwargs = dict(color="1", fontsize=1)
            axes_res[0].figure.suptitle(
                "Results displayed in Cartesian plane", **kwargs
            )
            axes_res[0].axis("off")
            axes_res[1].figure.suptitle(
                "Results displayed in polar plane", **kwargs
            )
            axes_res[1].axis("off")
            axes_res[1].set_xlim([0, dect_img.shape[1]])
            axes_res[2].figure.suptitle("Absolute radii", **kwargs)
            axes_res[2].set_xlabel("Angular coordinate [°]")
            axes_res[2].set_ylabel("Radial coordinate [pixels]")
            axes_res[3].figure.suptitle("Relative radii", **kwargs)
            axes_res[3].set_xlabel("Angular coordinate [°]")
            axes_res[3].set_ylabel("Radial coordinate [%]")
            for ax in axes_res:
                ax.grid("true")
                ax.legend()
            axes_showcase[0].figure.suptitle("Showcase", **kwargs)
            axes_showcase[0].set_title("Original image")
            axes_showcase[0].axis("off")
            axes_showcase[1].set_title("Support image")
            axes_showcase[1].axis("off")
            axes_showcase[-1].set_title("Obtained information")
            axes_showcase[-1].axis("off")
            axes_support[0].figure.suptitle("Support image", **kwargs)
            axes_support[0].axis("off")

        # Create plots directory
        if not os.path.isdir(dirpath_plots):
            os.makedirs(dirpath_plots)
            logger.info(f"Created folder(s) '{dirpath_plots}'")

        # Save all figures
        common.save_plots(logger, axes, dirpath_plots, dpi=300)
        logger.info(f"Saved plots to '{dirpath_plots}'")


if __name__ == "__main__":

    # Print docstring
    print(__doc__)

    # Path to default directory
    dirpath_default = os.path.abspath(
        os.path.join(os.path.dirname(__file__), os.path.pardir, "sample_rgb")
    )

    # Setup additional arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "path",
        nargs="?",
        default=dirpath_default,
        help="set path to target folder where to search for images (default: "
        "'%(default)s')",
    )
    arg_parser.add_argument(
        "-e",
        "--edit_only",
        action="store_true",
        help="do not process data after editing",
    )
    arg_parser.add_argument(
        "-p",
        "--process_only",
        action="store_true",
        help="do not edit data before processing",
    )
    arg_parser.add_argument(
        "-r",
        "--read_only",
        action="store_true",
        help="do not modify existing data (useful when resuming work after "
        "aborting)",
    )

    # General setup
    args, logger = common.setup("PyCreas", "pycreas", arg_parser)

    # Execute script
    main()
