#!/usr/bin/env python3

"""
    RelativeRadius: Calculates the relative radius of points within a polygon.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Let the relative radius of a point in a polygon be, with the polygon center
    as reference point, the distance to given point relative to the current
    polygon radius. Here, the polygon radius denotes the radial coordinate of
    the polygon expressed in polar form, that is, the representation of the
    polygon by defining the radius as a function of the angle. The relative
    radius is thus the ratio of length of two equidirectional lines that start
    in the polygon center and are bounded by given point and polygon side,
    respectively.

    This subprogram computes the relative radius of each point in the first
    given set of 2D points {pnts} within the polygon defined by the second given
    set of 2D points {poly}.

    To compute the relative radii, the following algorithm is executed:
    1) Calculation of the center {cntr} of the polygon {poly}.
    2) Transformation of all Cartesian coordinates to polar coordinates relative
    to the polygon center {cntr}:
        - Calculation of the distance to the center.
        - Calculation of the angle with the horizontal axis.
    3) Radial projection of each given point {pnts} onto the polygon sides by
    linear interpolation {proj}. Here, the angular components of the polar
    coordinates are required.
    4) Calculation of the relative radius using given points {pnts} and their
    projections {proj}. Here, the radial components of the polar coordinates
    are required.

    Step 3) can be performed only if the angular components of the polar
    coordinates of the polygon {poly} are (weakly) monotonic, that is, only
    increasing or only decreasing. Otherwise, determining the projection points
    {proj} is ambiguous.

    Such polygons are called invalid. Invalid polygons are automatically made
    valid by sorting their coordinates with respect to angle. This changes the
    original shape of the polygon, which is why the detection of an invalid
    polygon is reported.

    Convex polygons are always valid. Concave polygons can, but need not
    necessarily, be invalid. If the concave regions of an invalid polygon make
    up a relatively small part, the deviation from the original shape after
    making the polygon valid is negligible.
"""

import logging
import os.path
import argparse

import numpy as np
import matplotlib.pyplot as pl

import math_functions as mf
import common


logger = logging.getLogger("PyCreas").getChild(__name__)


class RelativeRadius:

    # Error codes
    SUCCESS = 0
    UNKNOWN_ERROR = 1
    POLYGON_INVALID = 2
    INPUT_ERROR = 4

    normalize_angle = np.vectorize(mf.normalize_angle_small)

    def get_polygon_center(self, pnts):

        return np.asarray(mf.get_polygon_center(pnts.tolist()))

    def cart_to_polar(self, pnts):

        return np.asarray([mf.cart_to_polar(pp) for pp in pnts.tolist()])

    def polar_to_cart(self, pnts):

        return np.asarray([mf.polar_to_cart(pp) for pp in pnts.tolist()])

    def weakly_increasing(self, vals):

        return mf.weakly_increasing(vals.tolist())

    def weakly_decreasing(self, vals):

        return mf.weakly_decreasing(vals.tolist())

    def get_intersection_lines(self, pA, pB, pC, pD):

        pA, pB, pC, pD = pA.tolist(), pB.tolist(), pC.tolist(), pD.tolist()

        return np.asarray([mf.get_intersection_lines(pA, pB, pC, pD)])

    def shift_angles(self, angs, start_ang, pos_direction):

        # Shift angles by full revolution depending on direction
        if pos_direction:
            angs[angs < start_ang] += 2 * np.pi
        else:
            angs[angs > start_ang] -= 2 * np.pi

    def main(self, pnts, poly):

        if np.size(pnts) < 2 or np.size(poly) < 6:
            logger.error("Check input arguments")
            return self.INPUT_ERROR, [np.array([np.nan])] * 7

        # Initialize return code
        returncode = self.SUCCESS

        # Compute center of polygon
        cntr = self.get_polygon_center(poly)

        # Perform coordinates transform
        pnts_polar = self.cart_to_polar(pnts - cntr)
        poly_polar = self.cart_to_polar(poly - cntr)

        # Check direction of polygon
        if np.sum(self.normalize_angle(np.diff(poly_polar[:, 1]))) > 0:
            pos_direction = True
        else:
            pos_direction = False

        # Shift angles with respect to direction
        start_ang = poly_polar[0, 1]
        self.shift_angles(poly_polar[:, 1], start_ang, pos_direction)
        self.shift_angles(pnts_polar[:, 1], start_ang, pos_direction)

        # Check if monotonic
        if pos_direction:
            if not self.weakly_increasing(poly_polar[:, 1]):
                returncode |= self.POLYGON_INVALID
        else:
            if not self.weakly_decreasing(poly_polar[:, 1]):
                returncode |= self.POLYGON_INVALID

        # Make polygon monotonic
        if returncode & self.POLYGON_INVALID:
            logger.info("Invalid polygon detected")
            sort_inds = np.argsort(poly_polar[:, 1])
            poly = poly[sort_inds, :]
            poly_polar = poly_polar[sort_inds, :]

        # Use angles to find indices of polygon vertices to which points belong
        vals = pnts_polar[:, 1]
        bins = poly_polar[:, 1]
        inds = np.digitize(vals, bins=bins, right=False)

        # # Check results
        # for ii in range(len(bins) + 1):
        #     if ii == 0:
        #         bin_range = [float("inf"), bins[ii]]  # increasing: -inf
        #     elif ii == len(bins):
        #         bin_range = [bins[ii - 1], float("inf")]  # decreasing: -inf
        #     else:
        #         bin_range = [bins[ii - 1], bins[ii]]
        #     vals_binned = vals[inds == ii]
        #     if len(vals_binned):
        #         bin_range = [f"{jj * 180/np.pi:.0f}" for jj in bin_range]
        #         vals_binned = [f"{jj * 180/np.pi:.0f}" for jj in vals_binned]
        #         bin_range = ",".join(bin_range)
        #         vals_binned = ",".join(vals_binned)
        #         logger.debug(f"Values [{vals_binned}]")
        #         logger.debug(f"   belong to bin [{bin_range})")

        # First and last bins are equivalent when digitizing angles
        inds[inds == len(bins)] = 0

        # Shift angles back
        pnts_polar[:, 1] = self.normalize_angle(pnts_polar[:, 1])
        poly_polar[:, 1] = self.normalize_angle(poly_polar[:, 1])

        # Perform radial projection of points onto the polygon sides
        proj = np.full(np.shape(pnts), np.nan)
        for ii in range(len(inds)):
            proj[ii, :] = self.get_intersection_lines(
                cntr, pnts[ii, :], poly[inds[ii] - 1, :], poly[inds[ii], :]
            )

        # Perform coordinates transform
        proj_polar = self.cart_to_polar(proj - cntr)

        # Compute relative radii
        rel_radii = pnts_polar[:, 0] / proj_polar[:, 0]

        # Pack results
        results = (
            rel_radii,
            cntr,
            pnts_polar,
            poly,
            poly_polar,
            proj,
            proj_polar,
        )

        return returncode, results


def main():

    # Name of script and path to its directory
    dirpath_script, filename_script = os.path.split(__file__)

    # Remove file extension
    filename_script, _ = os.path.splitext(filename_script)

    # Path to results directory
    dirpath_results = os.path.abspath(
        os.path.join(dirpath_script, f"_results_{filename_script}")
    )

    # Make up points
    pnts = [
        [0.2, 0.75],
        [0.4, 0.75],
        [0.5, 0.75],
        [0.6, 0.75],
        [0.8, 0.75],
    ]
    pnts = np.asarray(pnts)

    # Make up polygon
    if args.polygon_type == "random":
        poly = np.random.rand(20, 2)
        temp = poly - np.mean(poly, axis=0)
        inds = np.argsort(-np.arctan2(temp[:, 1], temp[:, 0]))
        poly = poly[inds]
    else:
        poly = [
            [1, 1],
            [0, 1],
            [0, 0],
            [1, 0],
            [1, 0.1],
            [1, 0.2],
            [1, 0.3],
            [1, 0.4],
            [1, 0.5],
            [1, 0.6],
            [1, 0.7],
            [1, 0.8],
            [1, 0.9],
        ]
        if args.polygon_type == "invalid":
            poly.extend(
                [
                    [1, 0.95],
                    [1.1, 0.95],
                    [1.1, 1],
                ]
            )
        poly = np.asarray(poly)

    # Create RelativeRadius object
    rel_rad = RelativeRadius()

    # Perform calculation
    returncode, results = rel_rad.main(pnts, poly)
    (
        rel_radii,
        cntr,
        pnts_polar,
        poly_modified,
        poly_polar,
        proj,
        proj_polar,
    ) = results

    # Initialize axes
    axes = [pl.subplots(layout="constrained")[1] for _ in range(3)]

    # Plot points
    axes[0].plot(
        pnts[:, 0],
        pnts[:, 1],
        linestyle="",
        marker=".",
        markersize=6,
        color="yellowgreen",
        label="pnts",
    )

    # Plot modified polygon
    if returncode:
        axes[0].plot(
            poly_modified[:, 0],
            poly_modified[:, 1],
            linestyle="dashed",
            linewidth=1,
            marker=".",
            markersize=6,
            color="mediumorchid",
            label="poly_modified",
        )

    # Plot polygon
    axes[0].plot(
        poly[:, 0],
        poly[:, 1],
        linestyle="dashed",
        linewidth=1,
        marker=".",
        markersize=6,
        color="deepskyblue",
        label="poly",
    )

    # Plot center
    axes[0].plot(
        cntr[0],
        cntr[1],
        linestyle="",
        marker="+",
        markersize=12,
        color="red",
        label="cntr",
    )

    # Plot projections
    axes[0].plot(
        proj[:, 0],
        proj[:, 1],
        linestyle="",
        marker=".",
        markersize=6,
        color="royalblue",
        label="proj",
    )

    # Plot lines between center and projections
    for pp in proj:
        axes[0].plot(
            [cntr[0], pp[0]],
            [cntr[1], pp[1]],
            linestyle="dotted",
            linewidth=1,
            color="silver",
        )

    # Plot absolute radii over angles
    axes[1].plot(
        pnts_polar[:, 1] * 180 / np.pi,
        pnts_polar[:, 0],
        linestyle="",
        marker=".",
        markersize=6,
        color="yellowgreen",
        label="pnts",
    )
    axes[1].plot(
        poly_polar[:, 1] * 180 / np.pi,
        poly_polar[:, 0],
        linestyle="",
        marker=".",
        markersize=6,
        color="mediumorchid" if returncode else "deepskyblue",
        label="poly",
    )
    axes[1].plot(
        proj_polar[:, 1] * 180 / np.pi,
        proj_polar[:, 0],
        linestyle="",
        marker=".",
        markersize=6,
        color="royalblue",
        label="proj",
    )

    # Plot relative radii over angles
    axes[2].plot(
        pnts_polar[:, 1] * 180 / np.pi,
        rel_radii * 100,
        linestyle="",
        marker=".",
        markersize=6,
        color="goldenrod",
        label="rel_radii",
    )

    # Figure setup
    axes[0].set_title("Results displayed in Cartesian plane")
    axes[0].set_xlabel("x-coordinate in m")
    axes[0].set_ylabel("y-coordinate in m")
    axes[0].axis("equal")
    axes[1].set_title("Results displayed in polar plane")
    axes[1].set_xlabel("Angular coordinate in °")
    axes[1].set_ylabel("Radial coordinate in m")
    axes[2].set_title("Relative radii displayed in polar plane")
    axes[2].set_xlabel("Angular coordinate in °")
    axes[2].set_ylabel("Radial coordinate in %")
    for ax in axes:
        ax.grid("true")
        ax.legend()

    # Create results directory
    if not os.path.isdir(dirpath_results):
        os.makedirs(dirpath_results)
        logger.info(f"Created folder(s) '{dirpath_results}'")

    # Save all figures
    logger.info("Saving plots to file ...")
    common.save_plots(logger, axes, dirpath_results, dpi=300)
    logger.info(f"Saved plots to '{dirpath_results}'")

    # Draw figure
    pl.show()


if __name__ == "__main__":

    # Print docstring
    print(__doc__)

    # Setup additional arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "polygon_type",
        nargs="?",
        default="valid",
        choices=["valid", "invalid", "random"],
        metavar="polygon_type",
        help="selects type between: %(choices)s (default: %(default)s)",
    )

    # General setup
    args, logger = common.setup("PyCreas", __file__, parser)

    # Execute script
    main()
