#!/usr/bin/env python3

"""
    IsletGui: Edits, saves, and loads data associated with a given image.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    This subprogram creates a graphical user interface (GUI) that allows the
    user to interactively draw a polygon on a given image. It is part of the
    semi-automatic determination of islet information, as the user-defined
    polygon serves as border of the islet. The GUI manages user-provided data by
    reading from and writing to a CSV textfile with the same name and location
    as the file containing the image.

    In addition to the main given image, a support image may be added to be
    automatically loaded into the GUI. The user may then draw the polygon onto
    the support image instead of the main image. The results are processed in
    the same way as if no support images were provided.

    Support images must be provided in a subfolder named 'support' with the same
    location as the main image. A support image is associated with the main
    image by its filename prefix: the first three characters of the name must
    match. If the prefix is not unique, the first match is used.

    For convenience, the contrast of the support image is increased for
    grayscale images. This is done only for display and has no further effect on
    the processing.

    Interactions aside from the default Matplotlib toolbar functions are:
    | User interaction                           | Function                |
    | ------------------------------------------ | ----------------------- |
    | Right mouse click on image (or hold)       | Draw polygon            |
    | Press backspace key (or hold)              | Undo previous action    |
    | Press escape key                           | Reset polygon           |
    | Press enter key or simply close the window | Save data and close GUI |
    | Enter Ctrl+C in the terminal               | Abort                   |
"""

import logging
import os.path
import argparse
import time

import numpy as np
import matplotlib.pyplot as pl
import matplotlib.colors as mcolors
import matplotlib as mpl

mpl.rcParams["keymap.back"] = ["left", "c"]

from cell_detection import CellDetection
import common

logger = logging.getLogger("PyCreas").getChild(__name__)


class PolygonBuilder:
    def __init__(self, fig, axes):

        self.fig = fig
        self.axes = axes
        self.cur_ax = axes[0]
        self.pathX, self.pathY = [], []
        self.background = None

        # Connect event to function
        canvas = self.fig.canvas
        self.cids = []
        self.cids.append(canvas.mpl_connect("motion_notify_event", self.onMove))
        self.cids.append(canvas.mpl_connect("button_press_event", self.onClick))
        self.cids.append(canvas.mpl_connect("button_release_event", self.onRel))
        self.cids.append(canvas.mpl_connect("key_press_event", self.onKeyPress))

    def set_background(self):

        line = self.cur_ax.get_lines()[0]
        canvas = self.fig.canvas

        # Turn on animation property
        line.set_animated(True)

        # Redraw figure
        canvas.draw()

        # Store everything but the current artist
        self.background = canvas.copy_from_bbox(self.cur_ax.bbox)

        # Redraw artist only
        self.cur_ax.draw_artist(line)

        # Blit redrawn area only
        canvas.blit(self.cur_ax.bbox)

    def reset_background(self):

        line = self.cur_ax.get_lines()[0]
        canvas = self.fig.canvas

        # Turn off animation property
        line.set_animated(False)

        # Reset background
        self.background = None

        # Redraw figure
        canvas.draw()

    def draw_blit(self):

        line = self.cur_ax.get_lines()[0]
        canvas = self.fig.canvas

        # Update data
        line.set_data(self.pathX, self.pathY)

        # Restore the background region
        canvas.restore_region(self.background)

        # Redraw artist only
        self.cur_ax.draw_artist(line)

        # Blit redrawn area only
        canvas.blit(self.cur_ax.bbox)

    def draw_all(self):

        # Update data
        for ax in self.axes:
            line = ax.get_lines()[0]
            line.set_data(self.pathX, self.pathY)

        # Redraw figure
        canvas = self.fig.canvas
        canvas.draw_idle()

    def onMove(self, event):

        if event.inaxes != self.cur_ax:
            return

        if self.background is not None:
            self.pathX.append(event.xdata)
            self.pathY.append(event.ydata)
            self.draw_blit()

    def onClick(self, event):

        if event.inaxes is None:
            return

        if event.button.name != "RIGHT":
            return

        self.cur_ax = event.inaxes

        self.pathX.append(event.xdata)
        self.pathY.append(event.ydata)
        self.set_background()
        self.draw_blit()

    def onRel(self, event):

        self.reset_background()
        self.draw_all()

    def onKeyPress(self, event):

        # Clear old data
        for ax in self.axes:
            if ax.lines[1:]:
                for obj in ax.lines[1:]:
                    ax.lines.remove(obj)
                self.draw_all()

        if event.key == "backspace":
            if len(self.pathX) and len(self.pathY):
                self.pathX.pop()
                self.pathY.pop()
            self.draw_all()

        if event.key == "escape":
            self.pathX, self.pathY = [], []
            self.draw_all()

        if event.key == "enter":

            # Disconnect all callbacks
            for cid in self.cids:
                self.fig.canvas.mpl_disconnect(cid)

            # Connect first and last point just for display
            if len(self.pathX) and len(self.pathY):
                self.pathX.append(self.pathX[0])
                self.pathY.append(self.pathY[0])
                self.draw_all()
                self.fig.canvas.flush_events()
                self.pathX.pop()
                self.pathY.pop()

            # Close figure
            time.sleep(0.5)
            pl.close(self.fig)


class IsletGui:

    # Error codes
    SUCCESS = 0
    UNKNOWN_ERROR = 1
    LOAD_DATA_ERROR = 2
    OPEN_IMAGE_ERROR = 4
    OPEN_SUPPORT_IMAGE_ERROR = 8
    ACCESS_ERROR = 16

    def _read(self, filepath_data):

        if os.path.isfile(filepath_data):
            with open(filepath_data, mode="r", encoding="utf-8") as file:
                data = np.loadtxt(file, dtype=np.int64, delimiter=";")
            return self.SUCCESS, data
        else:
            return self.LOAD_DATA_ERROR, np.empty(0, dtype=np.int64)

    def _write(self, filepath_data, data):

        with open(filepath_data, mode="w", encoding="utf-8") as file:
            np.savetxt(file, data, fmt="%d", delimiter=";")

    def load(self, filepath_img):

        # Load data
        filepath_data = common.replace_extension(filepath_img, "csv")
        returncode, data = self._read(filepath_data)

        # Check return code
        if returncode:
            logger.error(f"Error loading data from file '{filepath_data}'")
        else:
            logger.debug(f"Loaded data from file '{filepath_data}'")

        return returncode, data

    def _open_support(self, filepath_img):

        # Path to support directory
        head, tail = os.path.split(filepath_img)
        dirpath_spprt = os.path.join(head, "support")
        prefix = tail[:3]

        # Find support image with same prefix
        if os.path.isdir(dirpath_spprt):
            for file in sorted(os.listdir(dirpath_spprt)):
                if file.startswith(prefix):
                    filepath_spprt = os.path.join(dirpath_spprt, file)
                    img = common.my_imread(filepath_spprt)
                    if img is None:
                        logger.debug(f"Not an image: '{filepath_spprt}'")
                    else:
                        logger.debug(f"Opened image '{filepath_spprt}'")
                        img = common.normalize_contrast(img, limit=10)
                        return self.SUCCESS, (filepath_spprt, img)
            else:
                logger.debug(f"No support image found with prefix: '{prefix}'")
        else:
            logger.debug(f"No support folder found: '{dirpath_spprt}'")

        return self.OPEN_SUPPORT_IMAGE_ERROR, ("", None)

    def edit(self, filepath_img, read_only):

        # Initialize return code
        returncode = self.SUCCESS

        # Load data
        filepath_data = common.replace_extension(filepath_img, "csv")
        rcode, old_data = self._read(filepath_data)
        returncode |= rcode

        # Check if modifications are allowed
        if read_only and not returncode & self.LOAD_DATA_ERROR:
            logger.info(f"Not allowed to modify image '{filepath_img}'")
            returncode |= self.ACCESS_ERROR
            return returncode

        # Open image
        orig_img = common.my_imread(filepath_img)
        if orig_img is None:
            logger.error(f"Error opening image '{filepath_img}'")
            returncode |= self.OPEN_IMAGE_ERROR
            return returncode
        else:
            logger.debug(f"Opened image '{filepath_img}'")

        # Create CellDetection object
        cell_dect = CellDetection(orig_img)

        # Create mask
        mask = cell_dect.get_mask()

        # Create masked image
        color_array = [ii * 255 for ii in mcolors.to_rgb("yellowgreen")]
        dect_img = cell_dect.apply_mask(mask, color_array)

        # Open additional support images
        rcode, (filepath_spprt, spprt_img) = self._open_support(filepath_img)
        returncode |= rcode

        # Initialize axes
        if returncode & self.OPEN_SUPPORT_IMAGE_ERROR:
            ncols = 1
        else:
            ncols = 2
        fig, axes = pl.subplots(1, ncols, layout="constrained", squeeze=False)

        # Flatten axes
        axes = np.reshape(axes, -1)

        # Setup axes
        filepaths = (filepath_img, filepath_spprt)
        imgs = (dect_img, spprt_img)
        for ax, filepath, img in zip(axes[::-1], filepaths, imgs):

            # Initialize empty event line
            ax.plot([], linestyle="dashed", linewidth=2, color="deepskyblue")

            # Draw old data
            if np.size(old_data):
                ax.plot(
                    np.append(old_data[:, 0], old_data[0, 0]),
                    np.append(old_data[:, 1], old_data[0, 1]),
                    linestyle="dotted",
                    linewidth=2,
                    color="deepskyblue",
                )

            # Show image
            ax.imshow(img, cmap="gray")

            # Figure setup
            ax.set_title(os.path.basename(filepath))
            ax.axis("off")

        # Create PolygonBuilder object
        polygon_builder = PolygonBuilder(fig, axes)

        # Draw figure
        pl.show()

        # Remove duplicates
        new_data = np.array([polygon_builder.pathX, polygon_builder.pathY]).T
        inds = np.unique(new_data, return_index=True, axis=0)[1]
        new_data = new_data[np.sort(inds), :]

        # Save data if it contains at least 3 pairs of coordinates
        if np.size(new_data) >= 6:
            self._write(filepath_data, new_data)
            logger.info(f"Wrote data to file '{filepath_data}'")
        else:
            logger.info(f"No changes made to image '{filepath_img}'")

        return returncode


def main():

    # Create IsletGui object
    islet_gui = IsletGui()

    # Edit data
    returncode = islet_gui.edit(args.path_to_image, args.read_only)

    # Load data
    returncode, data = islet_gui.load(args.path_to_image)

    # Print results
    if not returncode:
        for line in repr(data).split("\n"):
            if line:
                logger.debug(line)


if __name__ == "__main__":

    # Print docstring
    print(__doc__)

    # Path to default directory
    dirpath_default = os.path.abspath(
        os.path.join(os.path.dirname(__file__), os.path.pardir, "sample_rgb")
    )

    # Path to default image
    filepath_default = os.path.join(dirpath_default, "01_image.tif")

    # Setup additional arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "path_to_image",
        nargs="?",
        default=filepath_default,
        help="set path to image (default: '%(default)s')",
    )
    arg_parser.add_argument(
        "-r",
        "--read_only",
        action="store_true",
        help="do not modify existing data (useful when resuming work after "
        "aborting)",
    )

    # General setup
    args, logger = common.setup("PyCreas", __file__, arg_parser)

    # Execute script
    main()
