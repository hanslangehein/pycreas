"""
    Common: A Python module containing commonly used functions.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os.path
import re
import logging
import argparse

import cv2
import numpy as np


def convert_gray_to_rgb(img):

    img = cv2.normalize(
        img,
        None,
        alpha=0,
        beta=255,
        norm_type=cv2.NORM_MINMAX,
        dtype=cv2.CV_8U,
    )
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

    return img


def normalize_contrast(img, limit):

    # Read number of channels
    channels = img.shape[-1] if img.ndim == 3 else 0
    if channels == 1:
        img = np.squeeze(img)
        channels = 0

    if channels == 0:
        img = cv2.normalize(
            img,
            None,
            alpha=0,
            beta=255,
            norm_type=cv2.NORM_MINMAX,
            dtype=cv2.CV_8U,
        )
        if limit is None:
            img = cv2.equalizeHist(img)
        else:
            clahe = cv2.createCLAHE(clipLimit=limit)
            img = clahe.apply(img)

    return img


def my_imread(path_to_image, return_rgb_only=False):

    # Open image
    img = None
    if os.path.isfile(path_to_image):
        img = cv2.imread(path_to_image, cv2.IMREAD_UNCHANGED)
        if img is not None and np.size(img):

            # Read number of channels
            channels = img.shape[-1] if img.ndim == 3 else 0
            if channels == 1:
                img = np.squeeze(img)
                channels = 0

            # Gray image (show image using pl.imshow(img, cmap="gray"))
            if channels == 0:
                if return_rgb_only:
                    img = convert_gray_to_rgb(img)

            # RGB image (OpenCV follows BGR, Matplotlib follows RGB order)
            elif channels == 3:
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            # RGBA image (OpenCV follows BGR, Matplotlib follows RGB order)
            elif channels == 4:
                if return_rgb_only:
                    img = cv2.cvtColor(img, cv2.COLOR_BGRA2RGB)
                else:
                    img = cv2.cvtColor(img, cv2.COLOR_BGRA2RGBA)

    # Return gray, RGB or RGBA image
    return img


def save_plots(logger, axes, dirpath="_savedPlots", figure_counter=0, **kwargs):

    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)

    figures = []
    titles = []
    for ax in axes:
        new_fig = ax.figure
        if new_fig not in figures:
            figures.append(new_fig)
            if new_fig._suptitle:
                title = new_fig._suptitle.get_text()
            else:
                title = ax.get_title()
            titles.append(title)

    for ii in range(len(figures)):

        title = re.sub(r"\W", "", titles[ii])
        figure_counter += 1

        for ext in ["pdf", "png", "tif"]:

            filename = f"{figure_counter:02d}_{title:s}.{ext}"
            filepath = os.path.abspath(os.path.join(dirpath, filename))

            figures[ii].savefig(filepath, **kwargs)

            logger.debug(f"Created figure '{filepath}'")

    return figure_counter


def replace_extension(filepath, extension):

    root, _ = os.path.splitext(filepath)
    filepath = f"{root}.{extension}"

    return filepath


def setup(rootname, path=__file__, parser=argparse.ArgumentParser()):

    # Check path
    if os.path.isdir(path):

        # Path to logs directory
        dirpath_logs = os.path.abspath(os.path.join(path, "_logs"))

        # Path to log file
        filepath_logs = os.path.join(dirpath_logs, f"{rootname}.log")

    else:

        # Name of script and path to its directory
        dirpath_script, filename_script = os.path.split(path)

        # Remove file extension
        filename_script, _ = os.path.splitext(filename_script)

        # Path to logs directory
        dirpath_logs = os.path.abspath(os.path.join(dirpath_script, "_logs"))

        # Path to log file
        filepath_logs = os.path.join(dirpath_logs, f"{filename_script}.log")

    # Parser setup
    parser.add_argument(
        "--debug", action="store_true", help="run this script in debug mode"
    )
    parser.add_argument(
        "--log", action="store_true", help=f"save logs to '{filepath_logs}'"
    )

    # Parse command-line arguments
    args = parser.parse_args()

    # Logger setup
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logtarget = ["screen", "file"] if args.log else ["screen"]
    if args.log and not os.path.isdir(dirpath_logs):
        os.makedirs(dirpath_logs)
        dirpath_logs_created = True
    else:
        dirpath_logs_created = False

    # Create logger
    create_logger(rootname, loglevel, logtarget, logpath=filepath_logs)
    if os.path.isdir(path):
        logger_ = logging.getLogger(rootname)
    else:
        logger_ = logging.getLogger(rootname).getChild(filename_script)
    if dirpath_logs_created:
        logger_.info(f"Created folder(s) '{dirpath_logs}'")

    # Print parsed arguments
    logger_.debug("Starting script using:")
    for aa in vars(args):
        logger_.debug(f"  {aa} = {getattr(args, aa)}")

    return args, logger_


def create_logger(
    name,
    levelname=logging.DEBUG,
    logtarget=["screen"],
    formatter=logging.Formatter(
        "%(levelname)8s|%(asctime)s|%(name)s: %(message)s", datefmt="%H:%M:%S"
    ),
    logpath=None,
    logstream=None,
):

    # Create or force update logger
    logger_ = logging.getLogger(name)
    if logger_.hasHandlers():
        logger_.handlers.clear()
    logger_.setLevel(levelname)

    # Disable logging messages to be passed to the handlers of ancestor loggers
    logger_.propagate = False

    if "screen" in logtarget:

        # Create console handler
        ch = logging.StreamHandler()
        ch.setLevel(levelname)

        # Add formatter to the handler
        ch.setFormatter(formatter)

        # Add the handler to logger
        logger_.addHandler(ch)

    if "file" in logtarget:

        # Create file handler
        fh = logging.FileHandler(filename=logpath, mode="w", encoding="utf-8")
        fh.setLevel(levelname)

        # Add formatter to the handler
        fh.setFormatter(formatter)

        # Add the handler to logger
        logger_.addHandler(fh)

    if "string" in logtarget:

        # Create console handler
        ch = logging.StreamHandler(logstream)
        ch.setLevel(levelname)

        # Add formatter to the handler
        ch.setFormatter(formatter)

        # Add the handler to logger
        logger_.addHandler(ch)

    return logger_
