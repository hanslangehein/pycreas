#!/usr/bin/env python3

"""
    CellDetection: Locates pixels of a given image using the HSV color space.
    Copyright (C) 2023  Hans Langehein

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    In this subprogram, an image thresholding operation is performed to obtain a
    mask that isolates pixels of a range of colors from the rest of a given
    image. The mask provides location of the detected pixels and is used as part
    of the semi-automatic determination of islet information, as the
    automatically detected pixels correspond to cells within the islet.

    The lower and upper thresholds are coded in HSV color space and adjusted to
    detect an earthy red-brown. If the given image is a grayscale image, light
    intensity levels are used as fallback thresholds.
"""

import logging
import os.path
import argparse

import numpy as np
import cv2
import matplotlib.pyplot as pl
import matplotlib.colors as mcolors

import common


logger = logging.getLogger("PyCreas").getChild(__name__)


class CellDetection:

    # Error codes
    SUCCESS = 0
    UNKNOWN_ERROR = 1

    def __init__(self, img):

        channels = img.shape[-1] if img.ndim == 3 else 0
        if channels == 0:
            self.img_type = "gray"
        elif channels == 1:
            img = np.squeeze(img)
            self.img_type = "gray"
        elif channels == 3:
            self.img_type = "rgb"
        elif channels == 4:
            img = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
            self.img_type = "rgb"
        else:
            logger.error(f"Unknown number of channels: {channels}")

        if self.img_type == "gray":
            img = common.convert_gray_to_rgb(img)

        self.rgb_img = img

    def get_mask(self):

        if self.img_type == "rgb":

            hsv_img = cv2.cvtColor(self.rgb_img, cv2.COLOR_RGB2HSV)

            lbound = np.array([5, 70, 20])
            ubound = np.array([15, 255, 255])
            mask = cv2.inRange(hsv_img, lbound, ubound)

        elif self.img_type == "gray":

            lbound = np.ones(3) * 10
            ubound = np.ones(3) * 255
            mask = cv2.inRange(self.rgb_img, lbound, ubound)

        return mask

    def apply_mask(
        self, mask, color_array=np.array([255, 255, 255], dtype=np.uint8)
    ):

        color_img = np.zeros(np.shape(self.rgb_img), dtype=np.uint8)
        color_img[:] = color_array

        background = cv2.bitwise_and(
            self.rgb_img, self.rgb_img, mask=cv2.bitwise_not(mask)
        )
        foreground = cv2.bitwise_and(color_img, color_img, mask=mask)

        img = cv2.add(background, foreground)

        return img


def main():

    # Name of script and path to its directory
    dirpath_script, filename_script = os.path.split(__file__)

    # Remove file extension
    filename_script, _ = os.path.splitext(filename_script)

    # Path to results directory
    dirpath_results = os.path.abspath(
        os.path.join(dirpath_script, f"_results_{filename_script}")
    )

    # Open image
    orig_img = common.my_imread(args.path_to_image)
    if orig_img is None:
        logger.error(f"Error opening image '{args.path_to_image}'")
        return

    # Create CellDetection object
    cell_dect = CellDetection(orig_img)

    # Create mask
    mask = cell_dect.get_mask()

    # Get cell area
    pnts = np.array(np.where(mask))
    pnts = np.array([pnts[1, :], pnts[0, :]]).T
    area = np.shape(pnts)[0]
    logger.info(f"Detected cells: {area}")

    # Create masked image
    color_array = [ii * 255 for ii in mcolors.to_rgb("yellowgreen")]
    dect_img = cell_dect.apply_mask(mask, color_array)

    # Initialize axes
    axes = [pl.subplots(layout="constrained")[1] for _ in range(2)]

    # Show images
    axes[0].imshow(orig_img, cmap="gray")
    axes[1].imshow(dect_img, cmap="gray")

    # Figure setup
    axes[0].set_title("Original image")
    axes[1].set_title("Detected cells")
    for ax in axes:
        ax.axis("off")

    # Create results directory
    if not os.path.isdir(dirpath_results):
        os.makedirs(dirpath_results)
        logger.info(f"Created folder(s) '{dirpath_results}'")

    # Save all figures
    logger.info("Saving plots to file ...")
    common.save_plots(logger, axes, dirpath_results, dpi=300)
    logger.info(f"Saved plots to '{dirpath_results}'")

    # Draw figure
    pl.show()


if __name__ == "__main__":

    # Print docstring
    print(__doc__)

    # Path to default directory
    dirpath_default = os.path.abspath(
        os.path.join(os.path.dirname(__file__), os.path.pardir, "sample_rgb")
    )

    # Path to default image
    filepath_default = os.path.join(dirpath_default, "01_image.tif")

    # Setup additional arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "path_to_image",
        nargs="?",
        default=filepath_default,
        help="set path to image (default: '%(default)s')",
    )

    # General setup
    args, logger = common.setup("PyCreas", __file__, arg_parser)

    # Execute script
    main()
