# **PyCreas**: A tool for quantification of localization and distribution of endocrine cell types in the islets of Langerhans.

## Table of Contents

- [**PyCreas**: A tool for quantification of localization and distribution of endocrine cell types in the islets of Langerhans.](#pycreas-a-tool-for-quantification-of-localization-and-distribution-of-endocrine-cell-types-in-the-islets-of-langerhans)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Functional specification](#functional-specification)
    - [CellDetection](#celldetection)
    - [IsletGui](#isletgui)
    - [RelativeRadius](#relativeradius)
  - [Showcase](#showcase)
  - [License](#license)
  - [Contact](#contact)

## Background

Islets of Langerhans are surrounded by exocrine tissue and consist of different endocrine cell types. PyCreas allows a systematic analysis of the distribution of endocrine cells within the islet.

The [first sample image](/sample_rgb/image.tif) shows an immunohistochemical staining for glucagon (brown) on a pancreatic section of a mouse. The [second sample image](/sample_gray/image.tif) shows an immunofluorescence staining for insulin (shown by grayscale) on a pancreatic section of a mouse.

## Installation

1. Ensure you can run Python and pip from the command line (see [Requirements for Installing Packages](https://packaging.python.org/en/latest/tutorials/installing-packages/#requirements-for-installing-packages)):
    ```bash
    python3 --version
    python3 -m pip --version
    ```
2. Clone or download this repository (see [Clone a repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) or download as zip via top right drop-down button of this page).
3. Navigate the command prompt into this repository's root directory using, e.g. '`cd pycreas`'.
4. Install extra dependencies:
    ```bash
    python3 -m pip install -r requirements.txt
    ```

## Usage

**PyCreas** can be run from this repository's root directory:

```bash
python3 pycreas_src path_to_folder
```

| Positional arguments | Function                                                                     |
| -------------------- | ---------------------------------------------------------------------------- |
| path_to_folder       | set path to target folder where to search for images (default: 'sample_rgb') |

| Optional arguments | Function                                                               |
| ------------------ | ---------------------------------------------------------------------- |
| -h, --help         | show help message and exit                                             |
| -e, --edit_only    | do not process data after editing                                      |
| -p, --process_only | do not edit data before processing                                     |
| -r, --read_only    | do not modify existing data (useful when resuming work after aborting) |
| --debug            | run this script in debug mode                                          |
| --log              | save logs to file                                                      |

For troubleshooting and to demonstrate the function of [CellDetection](#celldetection), [IsletGui](#isletgui), and [RelativeRadius](#relativeradius), each subprogram can be executed individually:

```bash
python3 pycreas_src/cell_detection.py path_to_image
```

```bash
python3 pycreas_src/islet_gui.py path_to_image
```

```bash
python3 pycreas_src/relative_radius.py polygon_type
```

Use the optional argument `--help` to get information about the command line arguments.

## Functional specification

**PyCreas** searches for images in a given target folder, obtains and analyzes
information contained in the images, and saves the results in a folder
located in the [pycreas_src](/pycreas_src/) directory.

To obtain and analyze information, the following subprograms are used:
- [CellDetection](#celldetection): Locates pixels of a given image using the HSV color space.
- [IsletGui](#isletgui): Edits, saves, and loads data associated with a given image.
- [RelativeRadius](#relativeradius): Calculates the relative radius of points within a polygon.

The process of obtaining information from images of islets of Langerhans is
semi-automatic and organized as follows:
1) Automatic detection of cells in the image (see [CellDetection](#celldetection)).
2) Manual definition of an islet border (see [IsletGui](#isletgui)).

The process of analyzing obtained information corresponding to a selected
islet of Langerhans includes:
1) Updating of detected cells by excluding positions outside the islet.
2) Counting of cell and islet pixels to obtain respective area measurements.
3) Calculation of relative area: ratio between cell pixel area and islet
pixel area.
4) Calculation of relative radius for each cell pixel (see [RelativeRadius](#relativeradius)).
5) Arithmetic averaging of relative radii less than or equal to 100% to
obtain an islet-specific quantification for the cell distribution.

Regarding step 5, it should be noted why relative radii greater than 100%
are excluded from averaging because, after all, the relative radius is
calculated exclusively for cells within the islet (see step 1). The
occurrence of relative radii greater than 100% is due to a modification of
the original islet shape. This modification is performed only for the
calculation of the relative radii. The modification is necessary by
definition and originates in making the islet shape valid (see
[RelativeRadius](#relativeradius)). How this affects the shape of an islet must be evaluated
individually for each islet. For this purpose, the percentage of excluded
relative radii is calculated to assess whether an islet is qualified for
this method. If the excluded relative radii percentage is low, only a small
proportion of cells are within invalid islet regions. Accordingly, only a
small proportion of relative radii are excluded from averaging, indicating
that the result is a representative quantification for the cell distribution.

The results are processed into a table with rows corresponding to the
different images and columns representing the following information:
- Cell pixel area
- Islet pixel area
- Cell-islet area ratio
- Average relative radius
- Excluded relative radii percentage

This table is then saved in the results folder as a CSV textfile and as an
XLSX spreadsheet. It is also used to generate plots, where each column
corresponds to a different plot. Additionally, plots are generated to
[visualize the obtained information](#showcase) from the last image in the specified
target folder.

Warnings are issued if the set of detected cells and the cells inside the
islet are disjoint.

### [CellDetection](/pycreas_src/cell_detection.py)

In this subprogram, an image thresholding operation is performed to obtain a
mask that isolates pixels of a range of colors from the rest of a given
image. The mask provides location of the detected pixels and is used as part
of the semi-automatic determination of islet information, as the
automatically detected pixels correspond to cells within the islet.

The lower and upper thresholds are coded in HSV color space and adjusted to
detect an earthy red-brown. If the given image is a grayscale image, light
intensity levels are used as fallback thresholds.

### [IsletGui](/pycreas_src/islet_gui.py)

This subprogram creates a graphical user interface (GUI) that allows the
user to interactively draw a polygon on a given image. It is part of the
semi-automatic determination of islet information, as the user-defined
polygon serves as border of the islet. The GUI manages user-provided data by
reading from and writing to a CSV textfile with the same name and location
as the file containing the image.

In addition to the main given image, a support image may be added to be
automatically loaded into the GUI. The user may then draw the polygon onto
the support image instead of the main image. The results are processed in
the same way as if no support images were provided.

Support images must be provided in a subfolder named 'support' with the same
location as the main image. A support image is associated with the main
image by its filename prefix: the first three characters of the name must
match. If the prefix is not unique, the first match is used.

For convenience, the contrast of the support image is increased for
grayscale images. This is done only for display and has no further effect on
the processing.

Interactions aside from the default Matplotlib toolbar functions are:
| User interaction                           | Function                |
| ------------------------------------------ | ----------------------- |
| Right mouse click on image (or hold)       | Draw polygon            |
| Press backspace key (or hold)              | Undo previous action    |
| Press escape key                           | Reset polygon           |
| Press enter key or simply close the window | Save data and close GUI |
| Enter Ctrl+C in the terminal               | Abort                   |

### [RelativeRadius](/pycreas_src/relative_radius.py)

Let the relative radius of a point in a polygon be, with the polygon center
as reference point, the distance to given point relative to the current
polygon radius. Here, the polygon radius denotes the radial coordinate of
the polygon expressed in polar form, that is, the representation of the
polygon by defining the radius as a function of the angle. The relative
radius is thus the ratio of length of two equidirectional lines that start
in the polygon center and are bounded by given point and polygon side,
respectively.

This subprogram computes the relative radius of each point in the first
given set of 2D points {pnts} within the polygon defined by the second given
set of 2D points {poly}.

To compute the relative radii, the following algorithm is executed:
1) Calculation of the center {cntr} of the polygon {poly}.
2) Transformation of all Cartesian coordinates to polar coordinates relative
to the polygon center {cntr}:
    - Calculation of the distance to the center.
    - Calculation of the angle with the horizontal axis.
3) Radial projection of each given point {pnts} onto the polygon sides by
linear interpolation {proj}. Here, the angular components of the polar
coordinates are required.
4) Calculation of the relative radius using given points {pnts} and their
projections {proj}. Here, the radial components of the polar coordinates
are required.

Step 3) can be performed only if the angular components of the polar
coordinates of the polygon {poly} are (weakly) monotonic, that is, only
increasing or only decreasing. Otherwise, determining the projection points
{proj} is ambiguous.

Such polygons are called invalid. Invalid polygons are automatically made
valid by sorting their coordinates with respect to angle. This changes the
original shape of the polygon, which is why the detection of an invalid
polygon is reported.

Convex polygons are always valid. Concave polygons can, but need not
necessarily, be invalid. If the concave regions of an invalid polygon make
up a relatively small part, the deviation from the original shape after
making the polygon valid is negligible.

## Showcase

This repository includes sample images. Running **PyCreas** or its subprograms without command line arguments will showcase the scope of the application. By default, the [rgb image](/sample_rgb/01_image.tif) contained in the folder [sample_rgb](/sample_rgb/) will be used. To select the [gray image](/sample_gray/01_image.tif) in the folder [sample_gray](/sample_gray/), set the `path_to_folder` command line argument accordingly (see [Usage](#usage)).

Among the results located in the [pycreas_src](/pycreas_src/) directory, the below images are generated to visualize how **PyCreas** obtains information from the [rgb](/sample_rgb/01_image.tif) and [gray image](/sample_rgb/01_image.tif), respectively:

![](/09_Showcase_rgb.png)

![](/09_Showcase_gray.png)

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](/LICENSE) for more details.

## Contact

If you are interested in contributing, please feel free to reach me out via email (see Git log) or submit a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

For further questions, please email [Stephan Scherneck](mailto:s.scherneck@tu-braunschweig.de).
